package me.projectx.Economy;

import me.projectx.Economy.Utils.Startup;

import org.bukkit.plugin.java.JavaPlugin;

/**
 * The Economy handler for Project-X
 *
 * @author Daniel S.
 * @version 1.0 Beta
 */
public class Main extends JavaPlugin {
	
	private static Main plugin;
	
	public void onEnable(){
		plugin = this;
		Startup.runStartup();
	}
	
	public static Main getInstance(){
		return plugin;
	}
}
