package me.projectx.Economy.Commands;

import java.sql.SQLException;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.projectx.Economy.Managers.AccountManager;
import me.projectx.Economy.Models.CommandModel;


public class PayCommand extends CommandModel {

	public PayCommand() {
		super("economy.pay", "/pay <player> <amount>");
	}

	@Override
	public boolean onCmd(CommandSender sender, String cml, String[] args) throws SQLException {
		if (args.length == 2){
			Player target = Bukkit.getPlayer(args[0]);
			AccountManager.getManager().withdraw((Player)sender, Double.parseDouble(args[1]));
			AccountManager.getManager().deposit(target, Double.parseDouble(args[1]));
			target.sendMessage(ChatColor.AQUA + sender.getName() + ChatColor.GRAY 
					+ " has paid you " + ChatColor.AQUA + "$" + args[1]);
			sender.sendMessage(ChatColor.GRAY + "Paid " + ChatColor.AQUA + "$" + args[1] 
					+ ChatColor.GRAY + " to " + ChatColor.AQUA + target.getName());
			return true;
		}else{
			return false;
		}
	}
}
