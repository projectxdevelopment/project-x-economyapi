package me.projectx.Economy.Commands;

import java.sql.SQLException;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.projectx.Economy.Managers.AccountManager;
import me.projectx.Economy.Models.CommandModel;

public class DepositCommand extends CommandModel {

	public DepositCommand() {
		super("economy.deposit", "/deposit <amount>");
	}

	@Override
	public boolean onCmd(CommandSender sender, String cml, String[] args) throws SQLException {
		if (args.length == 1){
			AccountManager.getManager().deposit((Player) sender, Double.parseDouble(args[0]));
			sender.sendMessage(ChatColor.AQUA + args[0] + ChatColor.GRAY + " has been deposited into your account.");
			return true;
		}else{
			return false;
		}
	}
}
