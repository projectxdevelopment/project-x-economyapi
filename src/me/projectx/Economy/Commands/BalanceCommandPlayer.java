package me.projectx.Economy.Commands;

import java.sql.SQLException;

import me.projectx.Economy.Managers.AccountManager;
import me.projectx.Economy.Models.CommandModel;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class BalanceCommandPlayer extends CommandModel {

	public BalanceCommandPlayer() {
		super("economy.balance.player", "/bal [player] [deposit <amount> || withdraw <amount]");		
	}

	@SuppressWarnings("deprecation")
	@Override
	public boolean onCmd(CommandSender sender, String cml, String[] args) throws SQLException {
		if (args.length == 0){		
			sender.sendMessage(ChatColor.GRAY + "Your current balance: " + ChatColor.AQUA + "$" + 
					AccountManager.getManager().getAccount((Player)sender).getBalance());
			return true;
		}else if (args.length == 1){	
			OfflinePlayer p = Bukkit.getPlayer(args[0]);
			if (p != null){
				sender.sendMessage(ChatColor.GRAY + args[0] + "'s current balance: " + 
					ChatColor.AQUA + AccountManager.getManager().getAccount((Player)p).getBalance());
				return true;
			}else{
				sender.sendMessage(ChatColor.RED + args[0] + " has never played before!");
				return true;
			}	
		}
		return false;
	}	
}
