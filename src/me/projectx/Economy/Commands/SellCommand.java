package me.projectx.Economy.Commands;

import java.sql.SQLException;

import me.projectx.Economy.Managers.AccountManager;
import me.projectx.Economy.Models.Account;
import me.projectx.Economy.Models.CommandModel;
import me.projectx.Economy.enums.SellItem;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class SellCommand extends CommandModel{

	public SellCommand() {
		super("economy.sell", "/sell");
	}

	@Override
	public boolean onCmd(CommandSender sender, String cml, String[] args) throws SQLException {
		Player p = (Player) sender;
		if (args.length == 0){
			ItemStack i = p.getInventory().getItem(p.getInventory().getHeldItemSlot());
			Account a = AccountManager.getManager().getAccount(p);
			switch(i.getType()){
				case DIAMOND:
					AccountManager.getManager().deposit(p, SellItem.DIAMOND.getPrice() * i.getAmount());
					p.getInventory().removeItem(i);
					p.sendMessage(ChatColor.GRAY + "Sold " + ChatColor.AQUA + "Diamond " + ChatColor.GRAY + "for " + ChatColor.AQUA + "$" + a.getLastDeposit());
					return true;
				case GOLD_INGOT:
					AccountManager.getManager().deposit(p, SellItem.GOLD.getPrice() * i.getAmount());
					p.getInventory().removeItem(i);
					p.sendMessage(ChatColor.GRAY + "Sold " + ChatColor.AQUA + "Gold Ingot " + ChatColor.GRAY + "for " + ChatColor.AQUA + "$" + a.getLastDeposit());
					return true;
				case IRON_INGOT:
					AccountManager.getManager().deposit(p, SellItem.IRON.getPrice() * i.getAmount());
					p.getInventory().removeItem(i);
					p.sendMessage(ChatColor.GRAY + "Sold " + ChatColor.AQUA + "Iron Ingot " + ChatColor.GRAY + "for " + ChatColor.AQUA + "$" + a.getLastDeposit());
					return true;
				case EMERALD:
					AccountManager.getManager().deposit(p, SellItem.EMERALD.getPrice() * i.getAmount());
					p.getInventory().removeItem(i);
					p.sendMessage(ChatColor.GRAY + "Sold " + ChatColor.AQUA + "Emerald " + ChatColor.GRAY + "for " + ChatColor.AQUA + "$" + a.getLastDeposit());
					return true;
				case REDSTONE:
					AccountManager.getManager().deposit(p, SellItem.REDSTONE.getPrice() * i.getAmount());
					p.getInventory().removeItem(i);
					p.sendMessage(ChatColor.GRAY + "Sold " + ChatColor.AQUA + "Redstone " + ChatColor.GRAY + "for " + ChatColor.AQUA + "$" + a.getLastDeposit());
					return true;
				case COAL:
					AccountManager.getManager().deposit(p, SellItem.COAL.getPrice() * i.getAmount());
					p.getInventory().removeItem(i);
					p.sendMessage(ChatColor.GRAY + "Sold " + ChatColor.AQUA + "Coal " + ChatColor.GRAY + "for " + ChatColor.AQUA + "$" + a.getLastDeposit());
					return true;
				default:
					p.sendMessage(ChatColor.DARK_RED + "You cannot sell that item! You can only sell" 
							+ ChatColor.YELLOW + " Diamonds, Emerald, Gold & Iron Ingots, Coal, and Redstone");
					return true;
			}
		}else{
			return false;
		}
	}
}
