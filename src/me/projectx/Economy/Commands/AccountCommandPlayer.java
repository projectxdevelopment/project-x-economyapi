package me.projectx.Economy.Commands;

import java.sql.SQLException;

import me.projectx.Economy.Managers.AccountManager;
import me.projectx.Economy.Models.Account;
import me.projectx.Economy.Models.CommandModel;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class AccountCommandPlayer extends CommandModel {

	public AccountCommandPlayer() {
		super("economy.account.use", "/account");
	}

	@Override
	public boolean onCmd(CommandSender sender, String cml, String[] args) throws SQLException {
		if (args.length == 0){
				Account a = AccountManager.getManager().getAccount((Player)sender);
				sender.sendMessage(ChatColor.GRAY + "Your current balance: " + ChatColor.AQUA + "$" + a.getBalance());
				sender.sendMessage(ChatColor.GRAY + "Your last deposit: " + ChatColor.AQUA + "$" + a.getLastDeposit());
				sender.sendMessage(ChatColor.GRAY + "Your last withdrawal: " + ChatColor.AQUA + "$" + a.getLastWithdrawal());
				return true;
		}else{
			return false;
		}
	}
}
