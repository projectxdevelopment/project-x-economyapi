package me.projectx.Economy.Commands;

import java.sql.SQLException;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.projectx.Economy.Managers.AccountManager;
import me.projectx.Economy.Models.CommandModel;

public class WithdrawCommand extends CommandModel {

	public WithdrawCommand() {
		super("economy.withdraw", "/withdraw <amount>");
	}

	@Override
	public boolean onCmd(CommandSender sender, String cml, String[] args) throws SQLException {
		if (args.length == 1){
			AccountManager.getManager().withdraw((Player)sender, Double.parseDouble(args[0]));
			sender.sendMessage(ChatColor.AQUA + args[0] + ChatColor.GRAY + " has been withdrawn from your account.");
			return true;
		}else{
			return false;
		}
	}
}
