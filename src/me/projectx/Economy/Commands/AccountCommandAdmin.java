package me.projectx.Economy.Commands;

import java.sql.SQLException;

import me.projectx.Economy.Managers.AccountManager;
import me.projectx.Economy.Models.CommandModel;
import me.projectx.Economy.enums.Message;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class AccountCommandAdmin extends CommandModel{

	public AccountCommandAdmin() {
		super("economy.admin", "/ecoadmin");
	}

	@Override
	public boolean onCmd(CommandSender sender, String cml, String[] args) throws SQLException {
		if (args.length > 0){
			switch(args[0]){
				case "reset":
					if (args.length == 2){
						OfflinePlayer p = Bukkit.getPlayer(args[1]);
						if (p != null){
							AccountManager.getManager().resetAccount((Player)p);
							sender.sendMessage(Message.ACCOUNT_RESET_SENDER.getMsg().replace("<player>", p.getName()));
							((CommandSender) p).sendMessage(Message.ACCOUNT_RESET.getMsg());
							return true;
						}
					}
					break;
				case "deposit":
					if (args.length == 3){
						OfflinePlayer p = Bukkit.getPlayer(args[1]);
						if (p != null){
							AccountManager.getManager().deposit(((Player)p), Double.parseDouble(args[2]));
							((Player)p).sendMessage(Message.ACCOUNT_ACTION.getMsg().replace("<amount>", args[2]).replace("<action>", "deposited into"));
							return true;
						}else{
							sender.sendMessage(Message.HAS_NOT_PLAYED.getMsg().replace("<player>", args[1]));
							return true;
						}
					}else{
						sender.sendMessage(ChatColor.GRAY + "Correct usage: " + ChatColor.AQUA + "/ecoadmin deposit <player> <amount>");
					}
					break;
				case "withdraw":
					if (args.length == 3){
						OfflinePlayer p = Bukkit.getPlayer(args[1]);
						if (p != null){
							AccountManager.getManager().withdraw(((Player) p), Double.parseDouble(args[2]));
							((CommandSender) p).sendMessage(Message.ACCOUNT_ACTION.getMsg().replace("<amount>", args[2]).replace("<action>", "withdrawn from"));
							return true;
						}else{
							sender.sendMessage(Message.HAS_NOT_PLAYED.getMsg().replace("<player>", args[1]));
							return true;
						}
					}else{
						sender.sendMessage(ChatColor.GRAY + "Correct usage: " + ChatColor.AQUA + "/ecoadmin withdraw <player> <amount>");
					}
					break;
				default:
					return false;
			}
		}
		return false;
	}
}
