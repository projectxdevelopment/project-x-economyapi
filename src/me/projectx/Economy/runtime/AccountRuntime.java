package me.projectx.Economy.runtime;

import java.util.Collections;

import me.projectx.Economy.Main;
import me.projectx.Economy.Managers.AccountManager;
import me.projectx.Economy.Models.Account;

import org.bukkit.scheduler.BukkitRunnable;

/**
 * The system for making the plugin run better
 * 
 * @author Daniel - TheGamingGrunts
 *
 */
public class AccountRuntime {
	
	private static AccountRuntime ar = new AccountRuntime();
	
	/**
	 * Get the runtime
	 * 
	 * @return The account runtime
	 */
	public static AccountRuntime getRuntime(){
		return ar;
	}
	
	/**
	 * Sorts the accounts based on their ID to allow faster random access
	 */
	public void sortAccounts(){
		new BukkitRunnable(){
			public void run(){
				Collections.sort(AccountManager.getManager().accounts, AccountManager.comparator);
			}
		}.runTaskAsynchronously(Main.getInstance());
	}
}
