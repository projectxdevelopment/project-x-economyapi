package me.projectx.Economy.enums;

import org.bukkit.ChatColor;

public enum Message {
	
	PREFIX("&8[&bTitanEco&8] "),
	ACCOUNT_RESET(PREFIX.getMsg() + "&7Your account has been reset!"),
	ACCOUNT_RESET_SENDER(PREFIX.getMsg() + "&7Reset &b<player>&7's account!"),
	ACCOUNT_ACTION(PREFIX.getMsg() + "&b<amount> &7 has been <action> your account"),
	HAS_NOT_PLAYED(PREFIX.getMsg() + "&c<player> &7has not played before!");
	
	private String msg;
	
	Message(String msg){
		this.msg = msg;
	}
	
	public String getMsg(){
		return ChatColor.translateAlternateColorCodes('&', msg);
	}
}
