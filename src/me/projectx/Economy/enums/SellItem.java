package me.projectx.Economy.enums;

import org.bukkit.Material;

public enum SellItem {
	
	DIAMOND(10, Material.DIAMOND),
	GOLD(6, Material.GOLD_INGOT),
	IRON(4, Material.IRON_INGOT),
	REDSTONE(1, Material.REDSTONE),
	COAL(2, Material.COAL),
	EMERALD(15, Material.EMERALD);
	
	private double price;
	private Material item;
	
	SellItem(double price, Material item){
		this.price = price;
		this.item = item;
	}
	
	/**
	 * Get the price of the item
	 * 
	 * @return The price of the item
	 */
	public double getPrice(){
		return price;
	}
	
	/**
	 * Get the material being sold
	 * 
	 * @return The material being sold
	 */
	public Material getItem(){
		return item;
	}
}
