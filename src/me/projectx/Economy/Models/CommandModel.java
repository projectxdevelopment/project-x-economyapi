package me.projectx.Economy.Models;

import java.sql.SQLException;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

/**
 * The basic model used to make command handling easier
 * 
 * @author Daniel - TheGamingGrunts
 */
public abstract class CommandModel implements CommandExecutor{

	private String perms, usage;
	
	public CommandModel(String permission, String usage){
		this.perms = permission;
		this.usage = usage;
	}
	
	public abstract boolean onCmd(CommandSender sender, String cml, String[] args) throws SQLException;
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args){
		if (!(sender.hasPermission(perms))){
			sender.sendMessage(ChatColor.DARK_RED + "You don't have permission to use that command :(");
			return false;
		}
		
		try {
			if (!(onCmd(sender, commandLabel, args))){
				sender.sendMessage(ChatColor.GRAY + "Correct usage: " + ChatColor.AQUA + getUsage());
				return false;
			}
		} catch (SQLException e) { e.printStackTrace();}
		return true;
	}

	/**
	 * Get the usage for the command
	 * 
	 * @return The command usage
	 */
	public String getUsage() {
		return usage;
	}
	
	/**
	 * Get the permission for the command
	 * 
	 * @return The permission for the command
	 */
	public String getPermission(){
		return perms;
	}
}
