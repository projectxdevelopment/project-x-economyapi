package me.projectx.Economy.Models;

import java.util.Comparator;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;

/**
 * Represents a player's account on the server
 * 
 * @author Daniel - TheGamingGrunts
 */
public class Account {
	
	private double balance, lastDeposit, lastWithdrawal;
	private UUID uuid;
	
	/**
	 * Create a new Account for a player
	 * 
	 * @param uuid : The UUID of a player
	 */
	public Account(UUID uuid){
		this.uuid = uuid;
	}
	
	/**
	 * Get the balance of the player
	 *
	 * @return The player's balance
	 */
	public double getBalance(){
		return balance;
	}
	
	/**
	 * Get the UUID of the player
	 * 
	 * @return The UUID of the player
	 */
	public UUID getId(){
		return uuid;
	}
	
	/**
	 * Get the last amount that a player deposited into their account
	 * 
	 * @return The last amount that the player deposited
	 */
	public double getLastDeposit(){
		return lastDeposit;
	}
	
	/**
	 * Set the amount of the player's last deposit
	 * 
	 * @param amount : The amount of the player's deposit
	 */
	public void setLastDeposit(double amount){
		this.lastDeposit = amount;
	}
	
	/**
	 * Get the last amount that a player withdrew from their account
	 * 
	 * @return The last amount that the player withdrew
	 */
	public double getLastWithdrawal(){
		return lastWithdrawal;
	}
	
	/**
	 * Set the amount of the player's last withdrawal
	 * 
	 * @param amount : The amount of the player's withdrawal
	 */
	public void setLastWithdrawal(double amount){
		this.lastWithdrawal = amount;
	}
	
	/**
	 * Set the balance of the player
	 * 
	 * @param amount : The amount to add on to the balance
	 */
	public void setBalance(double amount){
		this.balance = amount;
	}
	
	/**
	 * Get the player associated with this account's UUID
	 * 
	 * @return The player associated with the UUID
	 */
	public OfflinePlayer getPlayer(){
		return Bukkit.getOfflinePlayer(uuid);
	}
	
	/**
	 * Reset the account to it's default state when first created
	 */
	public void reset(){
		this.balance = 0;
		this.lastDeposit = 0;
		this.lastWithdrawal = 0;
	}
}
