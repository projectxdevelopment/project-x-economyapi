package me.projectx.Economy.Utils;

import java.sql.SQLException;

import me.projectx.Economy.Main;
import me.projectx.Economy.Commands.*;
import me.projectx.Economy.Events.*;
import me.projectx.Economy.Managers.AccountManager;

public class Startup {
	
	public static void runStartup(){	
		Main m = Main.getInstance();
		m.saveDefaultConfig();
		
		DatabaseUtils.setupConnection();
		try {
			DatabaseUtils.setupMySQL();
		} catch(SQLException e) {
			e.printStackTrace();
		}

		AccountManager.getManager().loadAccountsFromDB();
		
		m.getCommand("bal").setExecutor(new BalanceCommandPlayer());
		m.getCommand("account").setExecutor(new AccountCommandPlayer());
		m.getCommand("ecoadmin").setExecutor(new AccountCommandAdmin());
		m.getCommand("deposit").setExecutor(new DepositCommand());
		m.getCommand("withdraw").setExecutor(new WithdrawCommand());
		m.getCommand("sell").setExecutor(new SellCommand());
		m.getCommand("pay").setExecutor(new PayCommand());
		
		m.getServer().getPluginManager().registerEvents(new PlayerJoin(), m);
	}
}
