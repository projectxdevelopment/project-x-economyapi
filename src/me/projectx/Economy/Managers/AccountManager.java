package me.projectx.Economy.Managers;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.UUID;

import me.projectx.Economy.Main;
import me.projectx.Economy.Models.Account;
import me.projectx.Economy.Utils.DatabaseUtils;
import me.projectx.Economy.runtime.AccountRuntime;

import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

/**
 * The manager that handles everything account-related
 * 
 * @author Daniel - TheGamingGrunts
 */
public class AccountManager {
	
	public List<Account> accounts = new ArrayList<Account>();
	private static AccountManager bm = new AccountManager();
	
	public static AccountManager getManager(){
		return bm;
	}
	
	/**
	 * Get the Account of a given player
	 * 
	 * @param player : The player
	 * @return The player's account
	 */
	public Account getAccount(Player player){
		for (Account b : accounts){
			if (b.getId().equals(player.getUniqueId())){
				return b;
			}
		}
		return null;
	}
	
	/**
	 * Create an account for a player
	 *
	 * @param player : The player who's account will be created
	 */
	public void createAccount(Player player){
		Account b = new Account(player.getUniqueId());
		b.setBalance(0);
		b.setLastDeposit(0);
		b.setLastWithdrawal(0);
		accounts.add(b);
		System.out.println("[EconomyAPI] Created a new account for: " + player.getName());
		System.out.println("[EconomyAPI] Account ID: " + b.getId());
		try {
			DatabaseUtils.queryOut("INSERT INTO account(player, balance, lastDeposit, lastWithdrawal)"
					+ "VALUES ('" + player.getUniqueId().toString() + "'," + b.getBalance() + "," + b.getLastDeposit() + "," + b.getLastWithdrawal() + ");");
		} catch(SQLException e) {
			e.printStackTrace();
		}	
		AccountRuntime.getRuntime().sortAccounts();
	}
	
	/**
	 * Delete a player's account
	 * 
	 * @param player : The player who's account will be deleted
	 */
	public void deleteAccount(Player player){
		accounts.remove(getAccount(player));
		try {
			DatabaseUtils.queryOut("DELETE FROM account WHERE player='" + player.getUniqueId().toString() + "';");
		} catch(SQLException e) {
			e.printStackTrace();
		}
		AccountRuntime.getRuntime().sortAccounts();
	}
	
	/**
	 * Load all the accounts from the database
	 */
	public void loadAccountsFromDB(){
		new BukkitRunnable(){
			@SuppressWarnings("deprecation")
			public void run() {
				try {
					ResultSet result = DatabaseUtils.queryIn("SELECT * FROM account;");
					while (result.next()){
						Account b = new Account(UUID.fromString(result.getString("player")));
						b.setBalance(result.getDouble("balance"));
						b.setLastDeposit(result.getDouble("lastDeposit"));
						b.setLastWithdrawal(result.getDouble("lastWithdrawal"));
						accounts.add(b);
						System.out.println("[EconomyAPI] Loaded Account: " + b.getPlayer().getName() + "; ID: " + b.getId());
					}
					AccountRuntime.getRuntime().sortAccounts();
				} catch(SQLException e) {e.printStackTrace();}
			}
		}.runTaskAsynchronously(Main.getInstance());
	}
	
	/**
	 * Deposit a given amount into a player's balance
	 * 
	 * @param player : The player who will receive money
	 * @param amount : The amount to deposit
	 */
	public void deposit(Player player, double amount){	
		Account a = getAccount(player);
		a.setBalance(a.getBalance() + amount);
		a.setLastDeposit(amount);
		try {
			DatabaseUtils.queryOut("UPDATE account SET balance=" + a.getBalance() + ", lastDeposit=" + 
					a.getLastDeposit() + " WHERE player='" + player.getUniqueId().toString() + "';");
		} catch(SQLException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Withdraw a given amount from a player's balance
	 * 
	 * @param player : The player who will lose money
	 * @param amount : The amount to withdraw
	 */
	public void withdraw(Player player, double amount){
		Account a = getAccount(player);
		a.setBalance(a.getBalance() - amount);
		a.setLastWithdrawal(amount);
		try{
			DatabaseUtils.queryOut("UPDATE account SET balance=" + a.getBalance() + ", lastWithdrawal=" + 
					a.getLastWithdrawal() + " WHERE player='" + player.getUniqueId() + "';");
		}catch(SQLException e){
			e.printStackTrace();
		}
	}	
	
	/**
	 * Reset a player's account to it's default state
	 * 
	 * @param player : The player whose account will be reset
	 */
	public void resetAccount(Player player){
		Account a = getAccount(player);
		a.reset();
		try{
			DatabaseUtils.queryOut("UPDATE account SET balance=" + a.getBalance() + ", lastDeposit=" + a.getLastDeposit() + 
					", lastWithdrawal=" + a.getLastWithdrawal() + " WHERE player='" + player.getUniqueId().toString() + "';");
		}catch(SQLException e){
			e.printStackTrace();
		}
	}
	
	/**
	 * Comparator used to compare accounts based on their ID
	 */
	public static Comparator<Account> comparator = new Comparator<Account>() {
		@Override
		public int compare(Account a1, Account a2) {
			return a1.getId().compareTo(a2.getId());
		}
	};
}
